namespace EmailSenderAPI.Integrations;
public interface IAuthService
{
    Task<string> GetTokenAsync();
}
