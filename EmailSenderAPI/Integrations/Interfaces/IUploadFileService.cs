namespace EmailSenderAPI.Integrations;
public interface IUploadFileService
{
    Task<byte[]> GetFileAsync(string fileName);
}
