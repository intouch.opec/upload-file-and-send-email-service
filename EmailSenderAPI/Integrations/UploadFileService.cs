using System.Net.Http.Headers;
using System.Text;

namespace EmailSenderAPI.Integrations;

public class UploadFileService : IUploadFileService
{
  private readonly IAuthService _authService;
  private readonly IHttpClientFactory _httpClientFactory;

  public UploadFileService(IAuthService authService, IHttpClientFactory httpClientFactory)
  {
    _authService = authService;
    _httpClientFactory = httpClientFactory;
  }

  public async Task<byte[]> GetFileAsync(string fileName)
  {
    var token = await _authService.GetTokenAsync();
    var httpClient = _httpClientFactory.CreateClient("UploadFile");
    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
    var response = await httpClient.GetAsync($"/api/FileUpload/{fileName}");
    return await response.Content.ReadAsByteArrayAsync();
  }

}