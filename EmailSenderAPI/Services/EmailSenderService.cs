using EmailSenderAPI.Model;
using EmailSenderAPI.Integrations;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;

namespace EmailSenderAPI.Services;
public class EmailSenderService : IEmailSenderService
{
  private readonly EmailConfiguration _emailConfig;
  private readonly IUploadFileService _uploadFileService;

  public EmailSenderService(EmailConfiguration emailConfig, IUploadFileService uploadFileService)
  {
    _emailConfig = emailConfig;
    _uploadFileService = uploadFileService;
  }

  public async Task SendEmail(MessageRequestModel message)
  {
    var emailMessage = await CreateEmailMessage(message);
    Send(emailMessage);
  }

  private async Task<MimeMessage> CreateEmailMessage(MessageRequestModel message)
  {
    var emailMessage = new MimeMessage();
    emailMessage.From.Add(new MailboxAddress(_emailConfig.From, _emailConfig.From));
    emailMessage.To.AddRange(message.To.Select(x => new MailboxAddress(x, x)));
    emailMessage.Subject = message.Subject;
    var multipart = new Multipart("mixed");
    multipart.Add(new TextPart(TextFormat.Plain)
    {
      Text = message.Content
    });

    foreach (var attachmentPath in message.AttachmentFileUrls)
    {
      multipart.Add(await GetFileAttachment(attachmentPath));
    }

    emailMessage.Body = multipart;

    return emailMessage;
  }

  private void Send(MimeMessage mailMessage)
  {
    using var client = new SmtpClient();
    try
    {
      client.Connect(_emailConfig.SmtpServer, _emailConfig.Port, true);
      client.AuthenticationMechanisms.Remove("XOAUTH2");
      client.Authenticate(_emailConfig.UserName, _emailConfig.Password);
      client.Send(mailMessage);
    }
    catch
    {
      throw;
    }
    finally
    {
      client.Disconnect(true);
      client.Dispose();
    }
  }

  private async Task<MimePart> GetFileAttachment(string fileName) {
    return new MimePart("application", "octet-stream")
    {
      Content = new MimeContent(new MemoryStream(await _uploadFileService.GetFileAsync(fileName))),
      ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
      FileName = Path.GetFileName(fileName)
    };
  }
}