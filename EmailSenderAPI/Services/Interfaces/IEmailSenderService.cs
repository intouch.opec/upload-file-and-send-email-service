
using EmailSenderAPI.Model;

namespace EmailSenderAPI.Services;
public interface IEmailSenderService
{
    Task SendEmail(MessageRequestModel message);
}