using Microsoft.AspNetCore.Mvc;
using EmailSenderAPI.Services;
using Microsoft.AspNetCore.Authorization;
using EmailSenderAPI.Model;

namespace EmailSenderAPI.EmailSenderController
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailSenderController : ControllerBase
    {
        private readonly IEmailSenderService _emailSenderService;

        public EmailSenderController(
            IEmailSenderService emailSenderService
            )
        {
            _emailSenderService = emailSenderService;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> TestSendEmail(MessageRequestModel message)
        {
            await Task.Run(() => _emailSenderService.SendEmail(message));
            return Ok();
        }
    }
}