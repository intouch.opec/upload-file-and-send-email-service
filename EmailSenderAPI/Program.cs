using EmailSenderAPI.Model;
using EmailSenderAPI.Integrations;
using EmailSenderAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;

var builder = WebApplication.CreateBuilder(args);
var emailConfig = builder.Configuration
        .GetSection("EmailConfiguration")
        .Get<EmailConfiguration>();

builder.Services.AddSingleton(GetEmailConfig(emailConfig)!);

builder.Services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(options =>
        {
            options.Authority = "https://dev-qocbhbdd2tb6ghnl.us.auth0.com/";
            options.Audience = "https://localhost:7288";
        });

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<IEmailSenderService, EmailSenderService>();
builder.Services.AddScoped<IAuthService, AuthService>();
builder.Services.AddScoped<IUploadFileService, UploadFileService>();

builder.Services.AddHttpClient("UploadFile", client =>
{
    client.BaseAddress = new Uri("https://localhost:7288");
});

builder.Services.AddHttpClient("Auth", client =>
{
    client.BaseAddress = new Uri("https://dev-qocbhbdd2tb6ghnl.us.auth0.com");
    client.DefaultRequestHeaders.Add("Accept", "application/json");
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();

app.UseCors(x => x
       .AllowAnyOrigin()
       .AllowAnyMethod()
       .AllowAnyHeader());

app.MapControllers();

app.Run();

static EmailConfiguration? GetEmailConfig(EmailConfiguration? emailConfig)
{
    return emailConfig;
}