using Xunit;
using EmailSenderAPI.Services;
using EmailSenderAPI.Model;
using Moq;

namespace UploadFileAndSendEmailService.Tests.Services
{
  public class EmailSenderServiceTest
  {
    private readonly IEmailSenderService _emailSenderService;
    
    public EmailSenderServiceTest()
    {
      var mockEmailSenderService = new Mock<IEmailSenderService>();
      mockEmailSenderService.Setup(x => x.SendEmail(It.IsAny<MessageRequestModel>()));
      _emailSenderService = mockEmailSenderService.Object;
    }

    [Fact]
    public void SendEmail_InputMessageRequestModel_ShouldBeNotError()
    {
      _emailSenderService.SendEmail(new MessageRequestModel()
      {
        To = new() { "intouch.opec@gmail.com" },
        Subject = "Noti upload Email",
        Content = "Noti upload Email",
        AttachmentFileUrls = new() { "image.png" },
      });
    }
  }
}