using UploadFileAPI.Services;
using Moq;
using Microsoft.AspNetCore.Http;

namespace UploadFileAndSendEmailService.Tests.Services
{
  public class StorageServiceTest
  {
    private readonly IStorageService _storageService;
    private readonly string _fileNotFound = "fileNotFound.png";
    private readonly string _image = "image.png";

    public StorageServiceTest()
    {
      var mockStorageService = new Mock<IStorageService>();
      mockStorageService.Setup(x => x.UploadAsync(It.IsAny<IFormFile>()));
      mockStorageService.Setup(x => x.GetFileAsync(_fileNotFound)).Throws<Exception>();
      mockStorageService.Setup(x => x.GetFileAsync(_image));
      _storageService = mockStorageService.Object;
    }

    [Fact]
    public async void UploadAsync_WhenInputIFormFile_ShouldBeUploadSuccess()
    {
      var formFileMock = new Mock<IFormFile>();
      await _storageService.UploadAsync(formFileMock.Object);
    }

    [Fact]
    public async void GetFileAsync_WhenFoundImage_ReturnFileStream()
    {
      var arrange = _image;
      await _storageService.GetFileAsync(arrange);
    }

    [Fact]
    public void GetFileAsync_WhenNotFound_ThrowException()
    {
      var arrange = _fileNotFound;
      Assert.ThrowsAsync<Exception>(() => _storageService.GetFileAsync(arrange));
    }
  }
}
