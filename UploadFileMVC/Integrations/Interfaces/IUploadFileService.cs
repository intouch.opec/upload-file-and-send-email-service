namespace UploadFileMVC.Integrations;
public interface IUploadFileService
{
    Task UploadFileAsync(IFormFile formFile);
}
