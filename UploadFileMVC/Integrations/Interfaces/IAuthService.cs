namespace UploadFileMVC.Integrations;
public interface IAuthService
{
    Task<string> GetTokenAsync();
}
