using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace UploadFileMVC.Integrations;
public class AuthService : IAuthService
{
  private readonly IHttpClientFactory _httpClientFactory;

  public AuthService(IHttpClientFactory httpClientFactory)
  {
    _httpClientFactory = httpClientFactory;
  }

  public async Task<string> GetTokenAsync()
  {
    var httpClient = _httpClientFactory.CreateClient("Auth");
    var data = new
    {
      client_id = "6audTn5igk2MOUj2zlmfJbdRGd6Ylh8G",
      client_secret = "P4RX6tmJrrPnIdnL7JOJnAagcqD27QADE00j65SwiOyp0eaNJxRiwgXc7usPXBYv",
      audience = "https://localhost:7288",
      grant_type = "client_credentials"
    };
    var json = Newtonsoft.Json.JsonConvert.SerializeObject(data);
    var content = new StringContent(json, Encoding.UTF8, "application/json");
    var response = await httpClient.PostAsync("/oauth/token", content);
    if (response.IsSuccessStatusCode)
    {
      var responseContent = await response.Content.ReadAsStringAsync();
      var responseObject = JsonConvert.DeserializeObject<ApiResponse>(responseContent);
      Console.WriteLine(responseObject!.access_token);
      return responseObject!.access_token;
    }
    return string.Empty;
  }
  public class ApiResponse
  {
    public string access_token { get; set; } = string.Empty;
  }
}