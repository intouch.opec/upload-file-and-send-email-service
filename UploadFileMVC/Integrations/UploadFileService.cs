using System.Net.Http.Headers;

namespace UploadFileMVC.Integrations;

public class UploadFileService : IUploadFileService
{
  private readonly IAuthService _authService;
  private readonly IHttpClientFactory _httpClientFactory;

  public UploadFileService(IAuthService authService, IHttpClientFactory httpClientFactory)
  {
    _authService = authService;
    _httpClientFactory = httpClientFactory;
  }

  public async Task UploadFileAsync(IFormFile formFile)
  {
    var token = await _authService.GetTokenAsync();
    var httpClient = _httpClientFactory.CreateClient("UploadFile");
    using var form = new MultipartFormDataContent();
    using var stream = formFile.OpenReadStream();
    var fileContent = new StreamContent(stream);
    fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
    {
        Name = "file",
        FileName = formFile.FileName,
    };
    form.Add(fileContent);
    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
    var response = await httpClient.PostAsync("/api/FileUpload", form);
    var responseContent = await response.Content.ReadAsStringAsync();
    Console.WriteLine(responseContent);
  }

}