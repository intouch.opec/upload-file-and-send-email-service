using UploadFileMVC.Playground;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Auth0.AspNetCore.Authentication;
using UploadFileMVC.Integrations;

var builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<CookiePolicyOptions>(options =>
 {
     options.MinimumSameSitePolicy = SameSiteMode.None;
 });

builder.Services.AddAuth0WebAppAuthentication(PlaygroundConstants.AuthenticationScheme, options =>
{
    options.Domain = builder.Configuration["Auth0:Domain"] ?? string.Empty;
    options.ClientId = builder.Configuration["Auth0:ClientId"] ?? string.Empty;
    options.ClientSecret = builder.Configuration["Auth0:ClientSecret"] ?? string.Empty;
})
.WithAccessToken(options =>
{
    options.Audience = builder.Configuration["Auth0:Audience"];
    options.UseRefreshTokens = true;

    options.Events = new Auth0WebAppWithAccessTokenEvents
    {
        OnMissingRefreshToken = async (context) =>
        {
            await context.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            var authenticationProperties = new LoginAuthenticationPropertiesBuilder().WithRedirectUri("/").Build();
            await context.ChallengeAsync(PlaygroundConstants.AuthenticationScheme, authenticationProperties);
        }
    };
});

builder.Services.AddAuth0WebAppAuthentication(PlaygroundConstants.AuthenticationScheme2, options =>
{
    options.Domain = builder.Configuration["Auth02:Domain"] ?? string.Empty;
    options.ClientId = builder.Configuration["Auth02:ClientId"] ?? string.Empty;
    options.ClientSecret = builder.Configuration["Auth02:ClientSecret"] ?? string.Empty;
    options.SkipCookieMiddleware = true;
    options.CallbackPath = "/callback2";
}).WithAccessToken(options =>
{
    options.Audience = builder.Configuration["Auth02:Audience"];
    options.UseRefreshTokens = true;

    options.Events = new Auth0WebAppWithAccessTokenEvents
    {
        OnMissingRefreshToken = async (context) =>
        {
            await context.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            var authenticationProperties = new LoginAuthenticationPropertiesBuilder().WithRedirectUri("/").Build();
            await context.ChallengeAsync(PlaygroundConstants.AuthenticationScheme2, authenticationProperties);
        }
    };
});

builder.Services.AddScoped<IAuthService, AuthService>();
builder.Services.AddScoped<IUploadFileService, UploadFileService>();

builder.Services.AddHttpClient("UploadFile", client =>
{
    client.BaseAddress = new Uri("https://localhost:7288");
});

builder.Services.AddHttpClient("Auth", client =>
{
    client.BaseAddress = new Uri("https://dev-qocbhbdd2tb6ghnl.us.auth0.com");
    client.DefaultRequestHeaders.Add("Accept", "application/json");
});

// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
