namespace UploadFileMVC.Models;
public class UserProfileViewModel
{
    public string Country { get; set; } = string.Empty;
    public string EmailAddress { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public string ProfileImage { get; set; } = string.Empty;
}