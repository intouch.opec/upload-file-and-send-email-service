﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using UploadFileMVC.Models;
using UploadFileMVC.Integrations;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;

namespace UploadFileMVC.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly IUploadFileService _uploadFileService;

    public HomeController(IUploadFileService uploadFileService, ILogger<HomeController> logger)
    {
        _logger = logger;
        _uploadFileService = uploadFileService;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }

    [HttpPost]
    [Authorize]
    public async Task<IActionResult> Index(IFormFile file)
    {
        await _uploadFileService.UploadFileAsync(file);
        ViewBag.Message = "File Uploaded Successfully!!";
        return View();
    }
}
