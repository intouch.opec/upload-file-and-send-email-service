using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UploadFileMVC.Models;
using Auth0.AspNetCore.Authentication;
using UploadFileMVC.Playground;

namespace UploadFileMVC.Controllers;

public class AccountController : Controller
{
  public async Task Login(string returnUrl = "/")
  {
    var authenticationProperties = new LoginAuthenticationPropertiesBuilder()
        .WithRedirectUri(returnUrl).Build();

    await HttpContext.ChallengeAsync(PlaygroundConstants.AuthenticationScheme, authenticationProperties);
  }

  public async Task Login2(string returnUrl = "/")
  {
    var authenticationProperties = new LoginAuthenticationPropertiesBuilder()
        .WithRedirectUri(returnUrl)
        .Build();

    await HttpContext.ChallengeAsync(PlaygroundConstants.AuthenticationScheme2, authenticationProperties);
  }

  [Authorize]
  public async Task Logout()
  {
    var authenticationProperties = new LogoutAuthenticationPropertiesBuilder()
        .WithRedirectUri(Url.Action("Index", "Home")!).Build();

    await HttpContext.SignOutAsync(PlaygroundConstants.AuthenticationScheme, authenticationProperties);
    await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
  }

  [Authorize]
  public async Task Logout2()
  {
    // Indicate here where Auth0 should redirect the user after a logout.
    // Note that the resulting absolute Uri must be whitelisted in the
    // **Allowed Logout URLs** settings for the client.
    var authenticationProperties = new LogoutAuthenticationPropertiesBuilder()
        .WithRedirectUri(Url.Action("Index", "Home")!)
        .Build();

    await HttpContext.SignOutAsync(PlaygroundConstants.AuthenticationScheme2, authenticationProperties);
    await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
  }

  [Authorize]
  public async Task<IActionResult> Profile()
  {
    var accessToken = await HttpContext.GetTokenAsync("access_token");
    var idToken = await HttpContext.GetTokenAsync("id_token");
    var refreshToken = await HttpContext.GetTokenAsync("refresh_token");
    return View(new UserProfileViewModel()
    {
      Name = User.Identity!.Name ?? string.Empty,
      EmailAddress = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value ?? string.Empty,
      ProfileImage = User.Claims.FirstOrDefault(c => c.Type == "picture")?.Value ?? string.Empty
    });
  }

  /// <summary>
  /// This is just a helper action to enable you to easily see all claims related to a user. It helps when debugging your
  /// application to see the in claims populated from the Auth0 ID Token
  /// </summary>
  /// <returns></returns>
  [Authorize]
  public IActionResult Claims()
  {
    return View();
  }

  public IActionResult AccessDenied()
  {
    return View();
  }
}