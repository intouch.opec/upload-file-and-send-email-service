using System.ComponentModel.DataAnnotations;

namespace UploadFileAPI.Model
{
  public class UploadFileRequestModel
  {
    [Required(ErrorMessage = "Please select a file.")]
    [MaxFileSize(5 * 1024 * 1024)]
    [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
    public IFormFile File { get; set; } = null!;
  }
}