using Microsoft.AspNetCore.Mvc;
using UploadFileAPI.Services;
using UploadFileAPI.Model;
using Microsoft.AspNetCore.Authorization;

namespace UploadFileAPI.FileUploadController
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileUploadController : ControllerBase
    {
        private readonly IStorageService _storageService;

        public FileUploadController(IStorageService storageService)
        {
            _storageService = storageService;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> UploadFile([FromForm] UploadFileRequestModel body)
        {
            await _storageService.UploadAsync(body.File);
            return Ok();
        }

        [HttpGet("{fileName}")]
        [Authorize]
        public async Task<IActionResult> GetFile(string fileName)
        {
            var fileStream = await _storageService.GetFileAsync(fileName);
            return File(fileStream, "application/octet-stream", fileName);
        }
    }
}