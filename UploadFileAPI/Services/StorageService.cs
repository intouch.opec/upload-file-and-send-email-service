using UploadFileAPI.Integrations;

namespace UploadFileAPI.Services;

public class StorageService : IStorageService
{
    private readonly IHostEnvironment _environment;
    private readonly ISendEmailService _sendEmailService;

    public StorageService(IHostEnvironment environment, ISendEmailService sendEmailService)
    {
        _environment = environment;
        _sendEmailService = sendEmailService;
    }
    
    public async Task UploadAsync(IFormFile formFile)
    {
        var uploadsFolder = Path.Combine(_environment.ContentRootPath, "uploads");
        Directory.CreateDirectory(uploadsFolder);
        var uniqueFileName = $"{Guid.NewGuid()}_{formFile.FileName}";
        var filePath = Path.Combine(uploadsFolder, uniqueFileName);
        using var stream = new FileStream(filePath, FileMode.Create);
        await formFile.CopyToAsync(stream);
        await _sendEmailService.SendEmaiAsync(new MessageRequestModel()
        {
            To = new List<string>() { "intouch.opec@gmail.com" },
            Subject = "Noti",
            Content = "Noti",
            AttachmentFileUrls = new List<string>() { uniqueFileName }
        });
    }

    public Task<FileStream> GetFileAsync(string fileName)
    {
        var filePath = Path.Combine(_environment.ContentRootPath, "uploads", fileName);
        if (!File.Exists(filePath))
        {
            throw new Exception();
        }
        var fileStream = File.OpenRead(filePath);
        return Task.FromResult(fileStream);
    }
}