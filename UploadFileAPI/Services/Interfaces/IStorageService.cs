namespace UploadFileAPI.Services;
public interface IStorageService
{
    Task UploadAsync(IFormFile formFile);
    Task<FileStream> GetFileAsync(string fileName);
}
