namespace UploadFileAPI.Integrations;
public interface ISendEmailService
{
    Task SendEmaiAsync(MessageRequestModel messafe);
}

public class MessageRequestModel
{
  public List<string> To { get; set; } = new List<string>();
  public string Subject { get; set; } = string.Empty;
  public string Content { get; set; } = string.Empty;
  public List<string> AttachmentFileUrls { get; set; } = new();
}