namespace UploadFileAPI.Integrations;
public interface IAuthService
{
    Task<string> GetTokenAsync();
}
