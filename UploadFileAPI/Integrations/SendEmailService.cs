using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;

namespace UploadFileAPI.Integrations;

public class SendEmailService : ISendEmailService
{
  private readonly IAuthService _authService;
  private readonly IHttpClientFactory _httpClientFactory;

  public SendEmailService(IAuthService authService, IHttpClientFactory httpClientFactory)
  {
    _authService = authService;
    _httpClientFactory = httpClientFactory;
  }

  public async Task SendEmaiAsync(MessageRequestModel messafe)
  {
    var token = await _authService.GetTokenAsync();
    var httpClient = _httpClientFactory.CreateClient("SendEmail");
    var json = JsonConvert.SerializeObject(messafe);
    var content = new StringContent(json, Encoding.UTF8, "application/json");
    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
    await httpClient.PostAsync("/api/EmailSender", content);
  }

}